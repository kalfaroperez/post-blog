﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PostBlog.Common.Enums;
using PostBlog.Common.IdentiyAccess;
using PostBlog.Common.Services.Interfaces;
using PostBlog.Common.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace PostBlog.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PosterController : Controller
    {
        private readonly IPostRepository _postRepository;
        private readonly ILogger<PosterController> _logger;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public PosterController(
            IPostRepository postRepository,
            ILogger<PosterController> logger,
            UserManager<ApplicationUser> userManager,
            IAuthenticationRepository authenticationService,
            IHttpContextAccessor httpContextAccessor)
        {
            _postRepository = postRepository;
            _userManager = userManager;
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpPost]
        [Authorize]
        [Route("list-pending-for-approval")]
        public IActionResult GetPostPendingForApproval()
        {
            var listPost = _postRepository.FindByCondition(t => t.PostState == nameof(StateEnum.PENDING_PUBLISH_APPROVAL)).ToList();
            List<PostModel> list = new List<PostModel>();
            if (listPost.Any())
            {
                foreach (var item in listPost)
                {
                    list.Add(new PostModel
                    {
                        PostId = item.PostId,
                        Autor = item.PostAutor,
                        SubmittedDate = item.AuditCreatedOnDate,
                        State = item.PostState
                    });
                }
            }
            
            return Ok(list);
        }

        [HttpPost]
        [Authorize]
        [Route("approve-post")]
        public IActionResult ApprovePost([FromBody]PostModel model)
        {
            var post = _postRepository.FindByCondition(t=> t.PostId == model.PostId).FirstOrDefault();
            post.PostState = nameof(StateEnum.APPROVED);
            _postRepository.Update(post);
            _postRepository.SaveChanges();
            return Ok(post);
        }

        [HttpPost]
        [Authorize]
        [Route("reject-post")]
        public IActionResult RejectPost([FromBody]PostModel model)
        {
            var post = _postRepository.FindByCondition(t => t.PostId == model.PostId).FirstOrDefault();
            post.PostState = nameof(StateEnum.REJECTED);
            _postRepository.Update(post);
            _postRepository.SaveChanges();
            return Ok(post);
        }

        [HttpPost]
        [Authorize]
        [Route("pending-for-post")]
        public IActionResult PendingForAprrobalPost([FromBody]PostModel model)
        {
            var post = _postRepository.FindByCondition(t => t.PostId == model.PostId).FirstOrDefault();
            post.PostState = nameof(StateEnum.PENDING_PUBLISH_APPROVAL);
            _postRepository.Update(post);
            _postRepository.SaveChanges();
            return Ok(post);
        }
    }
}