﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using PostBlog.Common.Context;
using PostBlog.Common.IdentiyAccess;
using PostBlog.Common.Models;
using PostBlog.Common.Services.Interfaces;
using PostBlog.Common.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostBlog.Common.Services
{
    public class AuthenticationRepository : IAuthenticationRepository
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger _logger;
        private readonly PostBlogContext _context;
        public AuthenticationRepository(
            PostBlogContext context,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILoggerFactory logger
            ) 
        {
            this._context = context;
            this._userManager = userManager;
            this._signInManager = signInManager;
            this._logger = logger.CreateLogger<AuthenticationRepository>();
        }
        public async Task<LoginResponseModel> Sigin(string username, string password)
        {

            var result = await _signInManager.PasswordSignInAsync(userName: username, password: password, isPersistent: false, lockoutOnFailure: false);
            if (result.Succeeded)
            {
                var user = await _userManager.FindByNameAsync(username);
                if (user != null)
                {
                    var userRoles = await _userManager.GetRolesAsync(user);
                    await _signInManager.RefreshSignInAsync(user);
                    return new LoginResponseModel { Status = result.Succeeded, Role = userRoles.FirstOrDefault(), User = user };
                }
                
            }
            return new LoginResponseModel { Status = result.Succeeded, Role = string.Empty };
        }

        public async void SigOut()
        {
            await _signInManager.SignOutAsync();
        }

        public string FindUserByEmail(string email)
        {
            var e = _userManager.FindByEmailAsync(email);
            return _userManager.FindByEmailAsync(email).Result.Id;
        }
    }
}
