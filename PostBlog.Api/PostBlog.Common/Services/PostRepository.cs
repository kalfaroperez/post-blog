﻿using PostBlog.Common.Context;
using PostBlog.Common.Models;
using PostBlog.Common.Repository;
using PostBlog.Common.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostBlog.Common.Services
{
    public class PostRepository : RepositoryBase<Post>, IPostRepository
    {
        private readonly PostBlogContext _context;
        public PostRepository(PostBlogContext repositoryContext): base(repositoryContext)
        {
            this._context = repositoryContext;
        }

        public Post FindById(string PostId)
        {
            var post = FindByCondition(x => x.PostNumber.Equals(PostId)).FirstOrDefault();
            return post;
        }
    }
}
