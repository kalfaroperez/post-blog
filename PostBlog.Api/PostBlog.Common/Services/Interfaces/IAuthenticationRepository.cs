﻿using PostBlog.Common.ViewModels;
using System;
using System.Threading.Tasks;

namespace PostBlog.Common.Services.Interfaces
{
    public interface IAuthenticationRepository
    {
        Task<LoginResponseModel> Sigin(string username, string password);
        void SigOut();

        string FindUserByEmail(string email);
    }
}
