﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostBlog.Common.Enums
{
    public enum StateEnum
    {
        PENDING_PUBLISH_APPROVAL,
        APPROVED,
        REJECTED,
        SUBMITTED
    }
}
