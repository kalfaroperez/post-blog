**Post Blog Test Zemoga**

A continuacion dejo las indicaciones para poder probar la siguiente Solución

**Paso 1 ** 
	Ejecutar script con Base de Datos llamado PostBlog.sql
**ConnectionString Azure	
Server=tcp:postblog20210825091933dbserver.database.windows.net,1433;Initial Catalog=PostBlog20210825091933_db;Persist Security Info=False;User ID=kalfaro;Password=vB0SFeb6M47onmPCCaln;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;
	
	
**Paso 2 ** 
	Ejecutar la solución **PostBlog.WebApp ** la cual es una MVC WebApp .NetCore para probar el Punto 1 de la prueba tecnica

**Paso 3 ** 
	Ejecutar la solución **PostBlog.WebApp ** la cual es una MVC WebApi .NetCore para cumplir el Punto 2 de la prueba tecnica
	
##Sample Crendeciales 
**Writer**
	rcastro
	Abc123$$

**Editor**
	oalfaro
	Abc123$$
	
**NOTA**
	Implemento Swagger en las Apis, para mirar su documentacion. 
	Tenga en cuenta que la autenticacion es basada en token JWT, para dar Autorización al uso de las APIs.
	Antes de ejecutar un Api por favor logearse y copiar el token para poder Autorizar la API.

##Api Punto 2 Prueba Tecnica
 **Query endpoint**
 https://localhost:44372/api/poster/pending-for-post
 
 
**Approval endpoint(s)**
https://localhost:44372/api/poster/reject-post
https://localhost:44372/api/poster/approve-post

**Login for API Generate Token**






